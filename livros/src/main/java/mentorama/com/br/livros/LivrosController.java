package mentorama.com.br.livros;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/livros")
public class LivrosController {

    @RolesAllowed("user")
    @GetMapping
    public List<String> findAll(){
        return Arrays.asList("livro 1", "livro 2", "livro 3");
    }
}
